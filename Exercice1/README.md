# TP1 BDD UQAC

## Prérequis

- Python (3.9 idéalement)
- PIP

## Installation

> ⚠️ Ce projet, pour fonctionner nécessite l'installation de plusieurs librairies : `beautifulsoup4` et `requests`. Deux options sont possibles : les installer globalemet sur l'ordinateur ou bien les installer dans un environnement virtuel. L'intérêt de l'environnement virtuel est que les librairies ne sont pas installées globalement sur l'ordinateur. 
>
> Si vous souhaitez installer les librairies globalement sur l'ordinateur, il suffit de taper les commandes suivantes : 
> `pip install beautifulsoup4` et `pip install requests`
> Il est aussi possible de les installer via le fichier `requirements.txt` avec la commande : `pip install -r requirements.txt`. Cette dernière commande installera automatiquement les librairies nécessaires
>
> Dans le cas ou vous voulez installer les librairies dans un environnement virtuel, il va tout d'abord falloir installer la librairie permettant de créer des environnements virtuels : `virtualenv` avec la commande suivante : `pip install virtualenv`.
> Ensuite, il ne reste plus qu'à suivre les étapes ci-dessous.

1. Création de l'environment virtuel

```bash
python -m venv venv
```

2. Activer l'environment virtuel

```bash
venv/Scripts/activate      # Windows
. ./venv/Scripts/activate  # Linux
```

3. Installation des dépendances

```bash
pip install -r requirements.txt
```

ou bien

```bash
pip install beautifulsoup4
pip install requests
```

Et voila !

Il ne reste plus qu'à lancer le fichier `main.py`
Une fois terminé, l'ensemble des sorts devraient apparaitre dans le fichier `spells.json`.
