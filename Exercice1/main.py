import requests
from bs4 import BeautifulSoup
import re
import json

import traceback

URL = "https://www.d20pfsrd.com/magic/all-spells/"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")

lists = soup.find_all("li", class_="page new parent")

spells = []

tmpSpell = ""

for key, li in enumerate(lists):

    try:
        # On récupère le tag <a> vers le sort
        link = li.findChildren("a")[0]


        # On récupère le lien avec le "href"
        _spellPage = requests.get(link["href"])
        _spellBS = BeautifulSoup(_spellPage.content, "html.parser")


        # Nom du sort
        spellName = _spellBS.find("h1").text

        # On récupère la balise <b> contenant le texte "Level" (most cases)
        # S'il n'existe pas, on récupère alors la balise <p> contenant le texte "Level" (exceptions)
        _findLevels = _spellBS.find("b", string="Level")
        if _findLevels is None:
            _spellLevelsDiv = _spellBS.find(
                lambda tag: tag.name == "p" and "Level" in tag.text
            ).text
        else:
            _spellLevelsDiv = _findLevels.parent.text

        # On supprime tout ce qui est avant "Level"
        _spellLevelsP = re.sub("^.*Level", "", _spellLevelsDiv)
        # On supprime toutes les parenthèses
        _spellLevelsP = re.sub("\(.*\)", "", _spellLevelsP)
        # On supprime tout ce qui est après la section "Level"
        _spellLevelsP = re.sub("[;A-Z].*", "", _spellLevelsP)
        # On met à jour les espaces
        _spellLevelsP = re.sub("\s", " ", _spellLevelsP)
        # On récupère les niveaux
        _spellLevelsP = re.findall("([a-zA-Z]+[a-zA-Z/ -]+ [0-9]+)+", _spellLevelsP)
        tmpSpell = _spellLevelsP

        # On crée la liste des classes associées au bon niveau
        spellLevels = {}
        if _spellLevelsP != "":
            spellList = _spellLevelsP
            spellList = list(map(lambda el: [el.strip()[:-2], el.strip()[-1]], spellList))
            for level in spellList:
                if "/" in level[0]:
                    tmp = level[0].split("/")
                    for characterClass in tmp:
                        spellLevels[characterClass.strip()] = int(level[1])
                else:
                    spellLevels[level[0].strip()] = int(level[1])

        try:
            _spellComponentsDiv = _spellBS.find("b", string="Components").parent.text
            # On supprime tout ce qui est avant Components
            _spellComponentsDiv = re.sub("^.*Components", "", _spellComponentsDiv, flags=re.MULTILINE | re.DOTALL)
            # On gère les espaces
            _spellComponentsDiv = re.sub("\s+", " ", _spellComponentsDiv)
            # On gère les /
            _spellComponentsDiv = re.sub("[ ]*/[ ]*", "/", _spellComponentsDiv)
            # On récupère chaque composants
            _spellComponentsDiv = re.findall("(AF|[VSFMD]+[/VSFMD]*)(?=[ ,;<(]|$)", _spellComponentsDiv)
            spellComponents = _spellComponentsDiv

        except Exception as componentsException:
            spellComponents = []

        try:
            _spellResistDiv = _spellBS.find("b", string="Spell Resistance").parent.text
            spellResist = True
        except Exception as spellResistException:
            spellResist = False

        spells.append({
            "name": spellName,
            "level": spellLevels,
            "components": spellComponents,
            "resistance": spellResist
        })

    except Exception as spellException:
        print(key, " -> ", li, " \n ", tmpSpell)
        print(traceback.format_exc())

f = open("spells.json", "w+")
f.write(json.dumps(spells, indent=3))
f.close()