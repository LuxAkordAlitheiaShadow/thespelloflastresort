import org.apache.spark.sql.SparkSession
import org.apache.log4j.{Level, Logger}


object SQLRequest extends App {
  //Pas de messages rouges partout
  Logger.getLogger("org").setLevel(Level.ERROR)

  //Création de la session Spark
  val spark = SparkSession
    .builder()
    .appName("Spell Research SQL")
    .config("spark.master", "local")
    .getOrCreate()

  import spark.implicits._

  //Lecture du JSON
  val spellCollection = spark.read.option("multiline", "true").json("spells.json")

  //Création d'une vue type SQL
  spellCollection.createOrReplaceTempView("spells")

  //Requete SQL triant sur le level et le composant V uniquement, avec tri croissant
  val filteredSpellCollection = spark.sql("SELECT name, resistance, get_json_object(to_json(spells.level), '$.wizard') AS levelWizard FROM spells WHERE spells.components = array('V') AND get_json_object(to_json(spells.level), '$.wizard') IS NOT NULL AND get_json_object(to_json(spells.level), '$.wizard') < 5 SORT BY levelWizard ASC")

  //Création d'une classe d'affichage
  case class cls_spell(name: String, resistance: Boolean, levelWizard: String)

  //Affichage des résultats
  filteredSpellCollection.as[cls_spell].foreach(t => println(s"${t.name} : Spell Resistance = ${t.resistance}, Level = ${t.levelWizard}"))
  println(s"Stats :\n Nombre de spells trouves : ${filteredSpellCollection.count()}")

}
