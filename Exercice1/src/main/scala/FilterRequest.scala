import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{array_contains, size}
import org.apache.log4j.{Level, Logger}

object FilterRequest extends App {

  //Pas de messages rouges partout
  Logger.getLogger("org").setLevel(Level.ERROR)

  //Création de la session Spark
  val spark = SparkSession
    .builder()
    .appName("Spell Research Filter")
    .config("spark.master", "local")
    .getOrCreate()

  import spark.implicits._

  //Lecture du JSON contenant les spells
  val spellCollection = spark.read.option("multiline", "true").json("spells.json")

  //Récupération des colonnes sur lesquelles on filtre
  val colComponents = spellCollection("components")
  val colWizard = spellCollection("level.wizard")

  //Filtre pour les sorts de composant V uniquement
  val componentFilter = array_contains(colComponents, "V") &&
    size(colComponents) === 1

  //Filtre sur le niveau du sort
  val levelFilter = colWizard <= 4

  //Application des filtres
  val filteredSpellCollection = spellCollection.filter(
    componentFilter &&
      levelFilter
  )

  //Ajout d'une colonne du level wizard pour l'affichage
  val filteredSpellCollectionForPrint = filteredSpellCollection.withColumn("lvlWizard", colWizard).sort(colWizard)

  //Création d'une classe d'affichage
  case class cls_spell(name: String, resistance: Boolean, lvlWizard: String)

  //Affichage des résultats
  filteredSpellCollectionForPrint.as[cls_spell].foreach(t => println(s"${t.name} : Spell Resistance = ${t.resistance}, Level = ${t.lvlWizard}"))
  println(s"Stats :\n Nombre de spells trouves : ${filteredSpellCollectionForPrint.count()}")
}
