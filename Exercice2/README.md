# Consigne d'utilisation

## Sous IntelliJ IDEA CE

Le programme `./src/main/scala/PageRank/PageRank.scala` est executable sous l'IDE JetBrain IntelliJ IDEA CE.

Après avoir copié le répertoire git, ouvrer le dossier `Exercice 2` comme projet sous IntelliJ. Veillez à bien utiliser le SKD openJDK-11.

Il suffira ensuite de faire un `run 'pageRank'` pour que l'IDE compile et execute le programme Scala.