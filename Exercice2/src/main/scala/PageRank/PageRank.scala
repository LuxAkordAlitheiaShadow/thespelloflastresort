import org.apache.spark.{SparkConf}
import org.apache.spark.SparkContext
import org.apache.spark.graphx.{Edge, EdgeContext, Graph, _}
import org.apache.spark.graphx.TripletFields

package PageRank
{

  import org.apache.log4j.{Level, Logger}

  class node(val id: Int, var pagerankValue: Double, val neighborLength: Int) extends Serializable
  {
    override def toString: String = s"pagerankValue : $pagerankValue"
  }

  class PageRankCalculation extends Serializable
  {
    def sendPagerankValue(context: EdgeContext[node, String, Double]): Unit =
    {
      // Sending pagerank
      context.sendToDst(context.srcAttr.pagerankValue/context.srcAttr.neighborLength)
      context.sendToSrc(0)
    }

    def calculateNewPagerank(pagerankValueA: Double, pagerankValueB: Double): Double =
    {
      // Returning the sum of the neighboring pagerank
      pagerankValueA + pagerankValueB
    }

    def applyNewPagerank(vid: VertexId, node: node, newPagerankValue: Double): node =
    {
      // Returning node with new pagerank
      new node(node.id, (1-0.85)+(0.85*newPagerankValue), node.neighborLength)
    }

    def executeCalculation(graph: Graph[node, String], numberIteration: Int, sparkContent: SparkContext): Graph[node, String] =
    {
      var nodesGraph = graph
      // Executing iterations
      for (iteration <- 1 to numberIteration)
      {
        println("Iteration : " + iteration)
        // Getting neighboring pagerank
        val fields = new TripletFields(true, false, false);
        val message = nodesGraph.aggregateMessages(
          // Map function
          sendPagerankValue,
          // Reduce function
          calculateNewPagerank,
          // Accessed fields of edge context
          fields
        )

        // Applying new pagerank to vertices
        nodesGraph = nodesGraph.joinVertices(message)(
          (vid, node, newPagerankValue) => applyNewPagerank(vid, node, newPagerankValue)
        )

        //Print iteration values
        nodesGraph.vertices.collect().sortBy(_._1).foreach(println(_))
        //        if (message)
      }
      // Returning graph with new pageranks
      nodesGraph
    }
  }


  object pageRank extends App
  {
    //Pas de messages rouges partout
    Logger.getLogger("org").setLevel(Level.ERROR)

    // Setting up Spark cluster
    val conf = new SparkConf()
      .setAppName("Pagerank Graph (5 nodes)")
      .setMaster("local[*]")
    val sparkContext = new SparkContext(conf)
    sparkContext.setLogLevel("ERROR")

    // Creating graph
    var nodesVertices = sparkContext.makeRDD(
      Array(
        (1L, new node(id = 1, pagerankValue = 1, neighborLength = 2)),
        (2L, new node(id = 2, pagerankValue = 1, neighborLength = 1)),
        (3L, new node(id = 3, pagerankValue = 1, neighborLength = 1)),
        (4L, new node(id = 4, pagerankValue = 1, neighborLength = 1))
      )
    )
    var nodesEdges = sparkContext.makeRDD(
      Array(
        Edge(1L, 2L, "1"), Edge(1L,3L,"2"),
        Edge(2L, 3L, "3"),
        Edge(3L, 1L, "4"),
        Edge(4L, 3L, "5")
      )
    )
    var nodesGraph = Graph(nodesVertices, nodesEdges)

    // Executing pagerank algorithm
    val algoPagerank = new PageRankCalculation()
    val result = algoPagerank.executeCalculation(nodesGraph, 20, sparkContext)
    // Printing final result
    println("Result :")
    result.vertices.collect().sortBy(_._1).foreach(println(_))
  }
}
