# 8INF803 Devoir 1


## Exercice 1 : TheSpellOfLastResort

### Crawler

Nous avons réalisé notre crawler en python en utilisant la librairie BeautifulSoup.

Le code main.py est présent dans le dossier Exercice 1. Pour le lancer se réferer au README de l'exercice 1.

Nous avons exporté les données crawlées en JSON dans spells.json selon cette structure :

```JSON
{
      "name": "Abeyance",
      "level": {
         "cleric": 2,
         "inquisitor": 2,
         "paladin": 2
      },
      "components": [
         "V",
         "S",
         "M",
         "DF"
      ],
      "resistance": true
   },
```

Nous avons choisi de définir le level pour chaque classe afin d'avoir le plus d'informations possible.

Un grand nombre de règles particulières a du être implémenté dû à l'inconsistence des données sur le site d20pfsd.com.


### Map/Reduce

Pour cette partie, nous avons implémenté la recherche d'un spell de niveau 4 en composante verbale seulement avec les filters d'apache Spark.
Nous nous basons sur deux champs de notre JSON : "components" et "level.wizard". Si le premier contient uniquement "V" pour verbale et que le second est inférieur ou égal à 4, alors notre enregistrement correspond parfaitement aux critères de notre très cher Pito en surchauffe.
En appliquant ces filtre, nous obtenons donc les 37 sorts suivants.
```
Flare : Spell Resistance = true, Level = 0
Grasp : Spell Resistance = true, Level = 0
Oath of Anonymity : Spell Resistance = false, Level = 0
Vacuous Vessel : Spell Resistance = true, Level = 0
Decrepit Disguise : Spell Resistance = true, Level = 1
Deja Vu : Spell Resistance = true, Level = 1
Desperate Weapon : Spell Resistance = true, Level = 1
Feather Fall : Spell Resistance = true, Level = 1
Flare Burst : Spell Resistance = true, Level = 1
Liberating Command : Spell Resistance = true, Level = 1
Mindlink : Spell Resistance = true, Level = 1
Night Blindness : Spell Resistance = true, Level = 1
Sundering Shards : Spell Resistance = true, Level = 1
Touch of Blindness : Spell Resistance = true, Level = 1
Wave Shield : Spell Resistance = false, Level = 1
Anti-Summoning Shield : Spell Resistance = true, Level = 2
Anticipate Thoughts : Spell Resistance = true, Level = 2
Apport Object : Spell Resistance = true, Level = 2
Blindness-Deafness : Spell Resistance = true, Level = 2
Blur : Spell Resistance = true, Level = 2
Buoyancy : Spell Resistance = true, Level = 2
Knock : Spell Resistance = true, Level = 2
Psychic Reading : Spell Resistance = true, Level = 2
Raven’s Flight : Spell Resistance = false, Level = 2
Silent Table : Spell Resistance = true, Level = 2
Steal Voice : Spell Resistance = true, Level = 2
Dweomer Retaliation : Spell Resistance = true, Level = 3
Anywhere But Here : Spell Resistance = true, Level = 4
Charm Monster : Spell Resistance = false, Level = 4
Dimension Door : Spell Resistance = true, Level = 4
Emergency Force Sphere : Spell Resistance = true, Level = 4
Energy Hack : Spell Resistance = false, Level = 4
Fleeting Memory : Spell Resistance = true, Level = 4
Fool’s Teleport : Spell Resistance = true, Level = 4
Hypercognition : Spell Resistance = false, Level = 4
Mindwipe : Spell Resistance = true, Level = 4
Shout : Spell Resistance = true, Level = 4
```

Après lecture des descriptions, nous pensons que la meilleure solution pour éviter de se retrouver avec un Pito rôti est le sort Dimension Door.

### SQL

Nous avons choisi d'utiliser Spark SQL en important les données dans un dataframe puis en effectuant une requête SQL.

Nous retrouvons les mêmes résultats qu'à la question précédente.

## Exercice 2 : PageRank

Le but de cette exercice est de mettre en application l’algorithme de Pagerank sur un jeu de données répartis.

Pour mettre en avant cette application, nous nous servons d’une infrastructure Apache Spark, avec  du code source Scala, ainsi que l’API GraphX pour mettre en forme le graphe de données.

Le graphe de données qui valide notre code Scala de pageranking est le suivant :

![GrapheEnonce](./rapportResources/grapheEnonce.png)

Tous les sommets commencent avec une valeur de pagerank initial à 1.0.

La formule du pagerank est la suivante, avec d, le dumping factor, égal à 0.85.

<img src="https://latex.codecogs.com/svg.latex?PR_{sommet}&space;=&space;(1-d)&space;&plus;&space;d*\sum_{1}^{nb\_voisins_{sommet}}&space;(PR_{sommet\_voisin}/&space;nb\_voisins_{sommet\_voisin})" title="PR_{sommet} = (1-d) + d*\sum_{1}^{nb\_voisins_{sommet}} (PR_{sommet\_voisin}/ nb\_voisins_{sommet\_voisin})" />

Après 20 itérations, les sommets du graphe de données doit avoir pour valeurs de pagerank les suivantes :

![GrapheEnonceResultat](./rapportResources/grapheEnonceResultat.png)

Le résultat de note code Scala est le suivant :

```
Result :
(1,pagerankValue : 1.4901259564203881)
(2,pagerankValue : 0.7832552713203321)
(3,pagerankValue : 1.57661877225928)
(4,pagerankValue : 0.15000000000000002)
```
On constate que notre code est valide.
